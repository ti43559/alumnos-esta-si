import { Router } from "express";
import { getAlumno, saveAlumno, updateAlumno, deleteAlumno} from "../Controllers/AlumnosController.js"
import { subirImagen } from "../Middleware/Storage.js"
import { verificar } from '../Middleware/Auth.js'

const rutas = Router()

rutas.get('/api/Alumnos',verificar, getAlumno)
rutas.get('/api/Alumnos/:id', getAlumno)
rutas.post('/api/Alumnos', subirImagen.single('foto'), saveAlumno)
rutas.put('/api/Alumnos/:id', subirImagen.single('foto'), updateAlumno)
rutas.delete('/api/Alumnos/:id', deleteAlumno)


export default rutas